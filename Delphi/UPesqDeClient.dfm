object PesquisaDeCliente: TPesquisaDeCliente
  Left = 0
  Top = 0
  Caption = 'PesquisaDeCliente'
  ClientHeight = 288
  ClientWidth = 510
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 168
    Top = 24
    Width = 223
    Height = 29
    Caption = 'Pesquisa de cliente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 96
    Width = 163
    Height = 22
    Caption = 'Codigo do Cliente :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 126
    Top = 148
    Width = 61
    Height = 22
    Caption = 'Nome :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object DBEdit1: TDBEdit
    Left = 206
    Top = 100
    Width = 161
    Height = 21
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 206
    Top = 148
    Width = 161
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 206
    Top = 187
    Width = 161
    Height = 38
    Caption = 'Pesquisar'
    TabOrder = 2
    OnClick = Button1Click
  end
end
