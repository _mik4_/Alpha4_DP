object LoginFuncionario: TLoginFuncionario
  Left = 0
  Top = 0
  Caption = 'LoginFuncionario'
  ClientHeight = 310
  ClientWidth = 561
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 136
    Top = 24
    Width = 248
    Height = 29
    Caption = 'Login de Funcionario'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 48
    Top = 104
    Width = 52
    Height = 22
    Caption = 'Login:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 48
    Top = 168
    Width = 59
    Height = 22
    Caption = 'Senha:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object DBEdit1: TDBEdit
    Left = 136
    Top = 108
    Width = 233
    Height = 21
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 136
    Top = 172
    Width = 233
    Height = 21
    TabOrder = 1
  end
  object BitBtn1: TBitBtn
    Left = 224
    Top = 216
    Width = 145
    Height = 33
    Caption = 'Entrar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
end
