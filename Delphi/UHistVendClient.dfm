object HistoricoDoCliente: THistoricoDoCliente
  Left = 0
  Top = 0
  Caption = 'HistoricoDoCliente'
  ClientHeight = 242
  ClientWidth = 527
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 104
    Top = 24
    Width = 335
    Height = 29
    Caption = 'Historico de venda do cliente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 88
    Width = 163
    Height = 22
    Caption = 'Codigo do Cliente :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 128
    Top = 136
    Width = 66
    Height = 22
    Caption = 'Nome : '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object DBEdit1: TDBEdit
    Left = 200
    Top = 92
    Width = 177
    Height = 21
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 200
    Top = 140
    Width = 177
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 200
    Top = 184
    Width = 177
    Height = 34
    Caption = 'Pesquisar'
    TabOrder = 2
    OnClick = Button1Click
  end
end
