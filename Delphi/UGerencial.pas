unit UGerencial;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  Vcl.Menus;

type
  TFGerencial = class(TForm)
    MainMenu1: TMainMenu;
    Administrao1: TMenuItem;
    Clientes1: TMenuItem;
    Administrao2: TMenuItem;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGerencial: TFGerencial;

implementation

{$R *.dfm}

uses UCadastroProdutos, UCadastroFuncionarios;

procedure TFGerencial.Button1Click(Sender: TObject);
begin
TCadastroFuncionarios.show;
end;

procedure TFGerencial.Button2Click(Sender: TObject);
begin
TCadastroProdutos.show;
end;

end.
